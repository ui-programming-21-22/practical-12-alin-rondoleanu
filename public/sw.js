const cacheName = 'sample-pwa';
const filesToCache = [
  '.',
  'initial.js',
  'script.js',
  'index.html',
  'gameplay.html',
  'screen.css',
  './practical-12-alin-rondoleanu/',
  './practical-12-alin-rondoleanu/initial.js',
  './practical-12-alin-rondoleanu/script.js',
  './practical-12-alin-rondoleanu/index.html',
  './practical-12-alin-rondoleanu/gameplay.html',
  './practical-12-alin-rondoleanu/screen.css',

];

self.addEventListener('install', async e => {
  const cache = await caches.open(cacheName);
  await cache.addAll(filesToCache);
  return self.skipWaiting();
});

self.addEventListener('activate', e => {
  self.clients.claim();
});

self.addEventListener('fetch', async e => {
  const req = e.request;
  const url = new URL(req.url);

  if (url.origin === location.origin) {
    e.respondWith(cacheFirst(req));
  } else {
    e.respondWith(networkAndCache(req));
  }
});

async function cacheFirst(req) {
  const cache = await caches.open(cacheName);
  const cached = await cache.match(req.url);
  return cached || fetch(req);
}

async function networkAndCache(req) {
  const cache = await caches.open(cacheName);
  try {
    const fresh = await fetch(req);
    await cache.put(req, fresh.clone());
    return fresh;
  } catch (e) {
    const cached = await cache.match(req);
    return cached;
  }
}
